######## ALB ############
resource "aws_lb" "pub-lb" {
  name               = "pub1-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.lb-sg.id}"]
  subnets            = var.subnets
  tags = {
    Name = "PUB-LB"
  }
}
##### ALB Target Group
resource "aws_alb_target_group" "lb-tg-pub-pra1" {
  health_check {
    interval            = var.interval
    path                = "/"
    protocol            = var.protocol
    timeout             = var.timeout
    healthy_threshold   = var.threshold
    unhealthy_threshold = var.unthreshold
  }
  name     = "pub-lb-tg-pra1"
  port     = var.port
  protocol = var.protocol
  vpc_id   = var.vpc_id
}
###### LB Listner #####
resource "aws_alb_listener" "lb-listner" {
  load_balancer_arn = aws_lb.pub-lb.arn
  port              = var.port
  protocol          = var.protocol
  default_action {
    target_group_arn = aws_alb_target_group.lb-tg-pub-pra1.arn
    type             = "forward"
  }
}

##### EC2 Security Group  ######
resource "aws_security_group" "lb-sg" {
  name        = "TF-lb-sg"
  description = "lb security group"
  vpc_id      = var.vpc_id



  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["27.59.230.138/32"]
  }



  ingress {
    description = "allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["165.225.122.101/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }



  tags = {
    Name = "TF-PUB-LB-Security-Group"
  }
}

