##### RDS Security Group  ######
resource "aws_security_group" "rds-sg" {
  name        = "TF-rds-sg"
  description = "RDS security group"
  vpc_id      = var.vpc_id
  ingress {
    description = "MYSQL/Aurora"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["103.162.196.218/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "TF-RDS-Security-Group"
  }
}

##### Database subnet Group  ######

resource "aws_db_subnet_group" "tf-db-subnet" {
  name        = "tf-db-subnet"
  description = " Subnet group for RDS instance creation"
  subnet_ids  = var.subnet_ids
  tags = {
    Name = "TF-DB subnet group"
  }
}
#### RDS Instance Creation ####
resource "aws_db_instance" "default" {
  allocated_storage       = var.storage          # gigabytes
  backup_retention_period = var.retention_period # in days
  db_subnet_group_name    = aws_db_subnet_group.tf-db-subnet.id
  engine                  = "mysql"
  engine_version          = "5.7"
  instance_class          = "db.t3.micro"
  multi_az                = false
  name                    = "mydb"
  parameter_group_name    = "default.mysql5.7" # if you have tuned it
  password                = var.db_password
  port                    = 3306
  publicly_accessible     = false
  storage_encrypted       = true # you should always do this
  storage_type            = "gp2"
  username                = var.db_username
  vpc_security_group_ids  = [aws_security_group.rds-sg.id]
  skip_final_snapshot     = true
}
