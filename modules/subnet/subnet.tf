resource "aws_subnet" "SRU-Public" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.cidr1
  availability_zone       = var.az[0]
  map_public_ip_on_launch = true

  tags = {
    Name = "TF-PRA-PUB1"
  }
}

resource "aws_subnet" "public-sru" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.cidr2
  availability_zone       = var.az[1]
  map_public_ip_on_launch = true

  tags = {
    Name = "TF-PRA-PUB2"
  }
}

#####################PRIVATE SUBNETS###########################
resource "aws_subnet" "PRA-Public" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.cidr3
  availability_zone       = var.az[0]

  tags = {
    Name = "TF-PRA-PRI1"
  }
}

resource "aws_subnet" "private-pra" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.cidr4
  availability_zone       = var.az[1]

  tags = {
    Name = "TF-PRA-PRI2"
  }
}



variable "az" {
  type    = list(any)
  default = ["ap-southeast-1b", "ap-southeast-1a"]
}
