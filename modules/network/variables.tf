variable "vpc_cidr" {

  default = "10.0.0.0/16"
}


variable "demo-subnet-public-1_cidr" {
  default = "10.0.0.0/24"


}
variable "demo-subnet-public-2_cidr" {
  default = "10.0.2.0/24"


}

variable "demo-subnet-private-1_cidr" {

  default = "10.0.1.0/24"


}
variable "instance_type" {
  default = "t2.micro"
}
variable "ami" {

  default = "ami-05b891753d41ff88f"

}


