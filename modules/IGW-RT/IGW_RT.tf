########################################PUBLIC#####################################

#################### Internet Gateway

resource "aws_internet_gateway" "demo-igw" {
  vpc_id = var.vpc_id
  tags = {
    Name = "TF-IGW-PRA"
  }
}

##############Public Route table####

resource "aws_route_table" "pra-public-crt" {
  vpc_id = var.vpc_id

  route {
    //associated subnet can reach everywhere
    cidr_block = var.table_cidr
    //CRT uses this IGW to reach internet
    gateway_id = aws_internet_gateway.demo-igw.id
  }

  tags = {
    Name = "TF-public-crt"
  }
}
###########SUBNET-ID ASSOCIATION PUBLIC SUBNETS
# Route Table Entry and Subnet Association

resource "aws_route_table_association" "tf-crta-public-subnet-1" {
  subnet_id      = var.subnet_id
  route_table_id = aws_route_table.pra-public-crt.id
}


# Route Table Entry and Subnet Association

resource "aws_route_table_association" "tf-crta-public-subnet-2" {
  subnet_id      = var.subnet2_id
  route_table_id = aws_route_table.pra-public-crt.id
}


#############################################################PRIVATE########################################

######################## Elastic ip
resource "aws_eip" "elastic_ip" {
  vpc = true
}

######################## NAT gateway
resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.elastic_ip.id
  subnet_id     = var.subnet3_id

  tags = {
    Name = "nat-gateway"
  }
}
###############Private route table####
resource "aws_route_table" "pra-private-crt" {
  vpc_id = var.vpc_id
  route {
    cidr_block = var.table_cidr
    gateway_id = aws_nat_gateway.nat_gateway.id
  }

  tags = {
    Name = "TF-private-crt"
  }
}

##############SUBNET-ID ASSOCIATION PRIVATE SUBNETS
############### Route Table Entry and Subnet Association

resource "aws_route_table_association" "tf-crta-private-subnet-1" {
  subnet_id      = var.subnet3_id
  route_table_id = aws_route_table.pra-private-crt.id
}

resource "aws_route_table_association" "tf-crta-private-subnet-2" {
  subnet_id      = var.subnet4_id
  route_table_id = aws_route_table.pra-private-crt.id
}


