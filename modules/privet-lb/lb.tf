######## ALB ############
resource "aws_lb" "pri-lb" {
  name               = "pri-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.lb-sg.id}"]
  subnets            = var.subnets
  tags = {
    Name = "PUB-LB"
  }
}
##### ALB Target Group
resource "aws_alb_target_group" "lb-tg-pri-pra1" {
  health_check {
    interval            = var.interval
    path                = "/"
    protocol            = var.protocol
    timeout             = var.timeout
    healthy_threshold   = var.threshold
    unhealthy_threshold = var.unthreshold
  }
  name     = "pri-lb-tg-pra1"
  port     = var.port
  protocol = var.protocol
  vpc_id   = var.vpc_id
}
###### LB Listner #####
resource "aws_alb_listener" "lb-listner" {
  load_balancer_arn = aws_lb.pri-lb.arn
  port              = var.port
  protocol          = var.protocol
  default_action {
    target_group_arn = aws_alb_target_group.lb-tg-pri-pra1.arn
    type             = "forward"
  }
}

##### EC2 Security Group  ######
resource "aws_security_group" "lb-sg" {
  name        = "pri-TF-lb-sg"
  description = "lb security group"
  vpc_id      = var.vpc_id



  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }



  tags = {
    Name = "TF-PRI-LB-Security-Group"
  }
}

